<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta charset="utf-8">
  <title>Contato - IFFood</title>
  <!-- Fonte de icones -->
  <link href="../_script/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet">
  <!-- font-family: 'Exo 2', sans-serif; -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Estilo  -->
  <link href="../_css/styleContato.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../_css/nav.css" />
  <link type="text/css" rel="stylesheet" href="../_css/style.css" />
  <link rel="stylesheet" href="../_bootstrap/css/bootstrap.min.css" />
</head>
<body id="page-top">
<!-- IFRAME HEADER -->
<iframe src="header.html" width="100%" frameborder="0" height="66px" class="iframeFooter"></iframe>
  <!-- Contato -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Contato</h2>
          <h3 class="section-subheading text-muted">Fale Conosco!!</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <form name="formContato" method="post">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input class="form-control" id="nome" type="text" name="nome" placeholder="Nome *" required data-validation-required-message="Por favor, digite seu Nome!">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="email" type="email" name="email" placeholder="Email *" required data-validation-required-message="Por favor, digite seu Email!">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" id="phone" type="tel" name="fone" placeholder="Telefone. Ex: (xx)x xxxx-xxxx *" required data-validation-required-message="Por favor, digite seu número de telefone!">
                  <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                  <input class="form-control" type="text" name="assunto" placeholder="Assunto *" required data-validation-required-message="Por favor, digite o assunto da mensagem!">
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea class="form-control" id="message" name="mensagem" placeholder="Mensagem *" required data-validation-required-message="Por favor, digite sua mensagem!"></textarea>
                  <p class="help-block text-danger"></p>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-lg-12 text-center">
                <div id="success"></div>
                <input type="submit" class="btn btn-primary btn-xl text-uppercase btnEnviaEmial" value="Enivar Mensagem" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Footer -->
  <iframe src="../_elements/footer.html" width="100%" frameborder="0" height="325px" class="iframeFooter"></iframe>
  <!-- Jquery -->
  <script src="../_script/vendor/jquery/jquery.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="../_script/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Script Validação e smtp -->
  <script src="../_script/jqBootstrapValidation.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="../_bootstrap/js/bootstrap.min.js"></script>
  <script src="../_script/contentEmail.js"></script>

</body>


</html>
