<!DOCTYPE html>
<html>
<head>
	<meta http-equiv = "X-UA-Compatible" content="IE-edge" /><!-- Frescura do Internet Explorer. ESTA TAG DEVE VIR ANTES DE TODAS OUTRAS COISAS-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<title>Meta Page</title>
	
	<!-- Importando Estilo,Fontes,Scripts-->
	<link rel="stylesheet" href="../_bootstrap/css/bootstrap.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet"><!-- font-family: 'Exo 2', sans-serif; -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../_css/style.css" />
	<link rel="stylesheet" type="text/css" href="../_css/nav.css" />
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<link rel="stylesheet" href="../_css/controle.css"/>
	<script type="text/javascript" src="../_script/controle.js"></script>
	
</head>
<body>

	<!-- aviso -->
	<div class = "container-fluid">
		<div id = "aviso" class = "row" style = "display:none;">	
			<div class = "col-xs-12 col-sm-7 col-sm-offset-2 col-lg-5 col-lg-offset-3">
				<div id = "text_aviso"><p></p></div>
			</div>
			<div class = "col-xs-2 col-xs-offset-5 col-sm-1 col-sm-offset-0 ">
				<div id = "div_icon_aviso">
					<i id ="icon_aviso" class ="material-icons"></i>
				</div>
			</div>		
			
		</div>
	</div>
	
	
	<!-- Cabeçalho De Navegação -->
	<header id="nav">
		<nav class="navbar navbar-default" id="container-nav">
			<div class = "container">	  
				<div class= "navbar-header" id="nav-div-button">
					<button id="nav-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barra-navegacao">
						<span class="sr-only">Alternar Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand" id="name-iffood">IFFood</a>
				</div>
				<div class = "collapse navbar-collapse" id = "barra-navegacao">		
					<ul class = "nav navbar-nav navbar-right" id="lista-navegacao">
						<li id="pg_home"><i class="material-icons icon_op">&#xE8CC;</i><a href="../">Cardápio</a></li>
						<li id="pg_history"><i class="material-icons icon_op">&#xE8A6;</i><a href="sobre.php">Sobre Nós</a></li>
						<li id="pg_contato"><i class="material-icons icon_op">&#xE0E1;</i><a href="contato.php">Contato</a></li>
					</ul>				
				</div>
			</div>
		</nav>	
	</header>
	
	<!-- Corpo do Site-->
	<section id="">	
		<div class = "container-fluid">
			<div class = "row">			
				<div class = "col-xs-12">
					<h1 id = "text_control" class = "titulo">Controle de Admin</h1>
				</div>
				<div class = "col-xs-12">
					<h3 class = "subtitulo">Apenas utilize esta página caso saiba o que está fazendo. Todas suas alterações serão registradas.</h3>
				</div>				
			</div>	
			<div class = "row" id = "row_trufas">			
				<div class = "col-xs-12">
					<h2 id = ""  class = "titulo">Alimentos</h2>
				</div>				
			</div>	
			
		</div>
		
		
		
		
		
		
		
		<!-- POPUPZIN DE LEI -->
		<div id="popup_controle" class="modal fade" role="dialog">
			<div class="modal-dialog">
			<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">						
						<h4 id = "titulo_popup" class="modal-title">Tem certeza?</h4>
					</div>
					<div class="modal-body">
						<div id="div_compra" class = "container-fluid">
							<div class = "row">
								<!--informacoes do pedido -->
								    
									<div id = "info_pedido" class = "col-xs-12"></div>		
									<div id = "info_dinheiro" class = "col-xs-12"></div>	

								<!-- Botoes de confirmação e cancelamento -->
								
								<div class = "col-xs-12">								
									<div id = "btn_aceitar_produto" type="button" class = "btn botao_popup" data-toggle="modal" data-target="#popup_produtos">Sim</div>
								</div>
								<div class = "col-xs-12">
									<div id="btn_cancelar_pedido" type="button" class="btn botao_popup" style="background-color:#f00;">Não</div>
								</div>

								

							</div>
						</div>					
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>
	
	</section>
    <footer>
		<!-- 		<div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; IFFood 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              
            </ul>
          </div>
        </div>
      </div> -->
    </footer>
	<!-- JS do Bootstrap (nao tirar daqui)-->
    <script type="text/javascript" src = "../_bootstrap/js/bootstrap.min.js"></script>
</body>
</html>