<?php
require_once "../../_core/_model/Vendedores.class.php";
if (isset($_POST["user"]) && isset($_POST["pass"])) {
  $user = $_POST["user"];
  $pass = $_POST["pass"];
  $v = new Vendedores();
  if ($v->auth($user, $pass) === false) {
    header("location: ../");
  } else {
    session_start();
    $_SESSION["user"] = $user;
    $_SESSION["pass"] = $pass;
    header("location: ../panel/");
  }
} else {
  header("location: ../");
}
