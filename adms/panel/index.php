<?php
  session_start();
  if (isset($_SESSION["user"]) && isset($_SESSION["pass"])) {
    $user = $_SESSION["user"];
    $pass = $_SESSION["pass"];
  } else {
    header("location: ../");
  }
?>
<!DOCTYPE html>
<html>
  <head>
	<meta http-equiv = "X-UA-Compatible" content="IE-edge" /><!-- Frescura do Internet Explorer. ESTA TAG DEVE VIR ANTES DE TODAS OUTRAS COISAS-->
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
    <title>Painel Vendedor</title>
    <?php echo "<script>var user = '$user', pass = '$pass';</script>"; ?>
	<!-- Importando Estilo,Fontes,Scripts-->
	<link rel="stylesheet" href="../../_bootstrap/css/bootstrap.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet"><!-- font-family: 'Exo 2', sans-serif; -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../../_css/nav.css" />
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src = "..\..\_script\painel_vendedor.js"></script>
	<link href="..\..\_css\painel_vendedor.css" rel="stylesheet">

	
  </head>
  <body>
	<!-- Cabeçalho De Navegação -->
	<header id="nav">
		<nav class="navbar navbar-default" id="container-nav">
			<div class = "container">	  
				<div class= "navbar-header" id="nav-div-button">
					<button id="nav-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barra-navegacao">
						<span class="sr-only">Alternar Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand" id="name-iffood">IFFood</a>
				</div>
				<div class = "collapse navbar-collapse" id = "barra-navegacao">		
					<ul class = "nav navbar-nav navbar-right" id="lista-navegacao">						
						<li id="pg_home"><i class="material-icons icon_op">&#xE8CC;</i><a href="../../">Cardápio</a></li>
						<li id="pg_history"><i class="material-icons icon_op">&#xE8A6;</i><a href="../../_elements/sobre.php">Sobre Nós</a></li>
						<li id="pg_contato"><i class="material-icons icon_op">&#xE0E1;</i><a href="../../_elements/contato.php">Contato</a></li>
						<li id="pg_home"><i class="material-icons icon_op">&#xE8B8;</i><a href="../../_elements/controle.php">Controle</a></li>								
						<li id="pg_logout"><i class="material-icons icon_op">&#xE8AC;</i><a href="logout.php">Logout</a></li>
				
					</ul>				
				</div>
			</div>
		</nav>	
	</header>
	
	<!--DIV AVISO GERIAS-->
	<div class = "container-fluid">
		<div id = "aviso" class = "row" style = "display:none;">	
			<div class = "col-xs-12 col-sm-7 col-sm-offset-2 col-lg-5 col-lg-offset-3">
				<div id = "text_aviso"><p>Confirmação de entrega realizada com sucesso</p></div>
			</div>
			<div class = "col-xs-2 col-xs-offset-5 col-sm-1 col-sm-offset-0 ">
				<div id = "div_icon_aviso">
					<i id ="icon_aviso" class ="material-icons"></i>
				</div>
			</div>	
		</div>
	</div>
	
	<!-- Corpo do Site-->
	<section id="corpo" style = "padding-bottom: 100px;">
		<div class = "container-fluid">
			<div id = "pedidos_pessoais" class = "row">	
				<div class = "col-xs-12">
					<h4 class = "titulo_card">Pedidos do Vendedor</h4>
				</div>				
				<!--  PEDIDOS DO VENDEDOR VIRÃO AQUI -->
				<div id = "aviso_pessoal" class = "pedido_pessoal col-xs-12 col-sm-10 col-sm-offset-1">
					<div>
						<h5>
							Você não tem pedidos para entregar :(
						</h5>						
					</div>
				</div>
				
				
			</div>
		</div>
		
		<div class = "container-fluid">
			<div id = "pedidos_gerais" class = "row">
				<div class = "col-xs-12">
					<h4 class = "titulo_card" style = "margin-top:60px;">Pedidos Gerais</h4>					
				</div>				
				

				<!--  PEDIDOS GERAIS VIRÃO AQUI	 -->
			</div> 
		</div>
		
		
		
		<!--botão que ativa popup-->
		
		<!-- POPUPZIN DE LEI -->
		<div id="popup_produtos" class="modal fade" role="dialog">
			<div class="modal-dialog">
			<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">						
						<h4 id = "titulo_popup" class="modal-title">Informações Finais</h4>
					</div>
					<div class="modal-body">
						<div id="div_compra" class = "container-fluid">
							<div class = "row">
								<!--informacoes do pedido -->
								
									<div id = "info_pedido" class = "col-xs-12">
									
										<div class = "infos_popup" id = "local"></div>
										<div class = "infos_popup" id = "complemento"></div>
										<div class = "infos_popup" id = "cliente"></div>
										<div class = "infos_popup" id = "produtos"></div>
										
									</div>
									
								





								<!-- Botoes de confirmação e cancelamento -->
								
								<div class = "col-xs-12">
									
									<div style = "display:none; background-color:#0f0;" id = "btn_confirmar_entrega_produto" type="button" class = "btn botao_popup" data-toggle="modal" data-target="#popup_produtos">Confirmar Entrega</div>
									<div id = "btn_aceitar_produto" type="button" class = "btn botao_popup" data-toggle="modal" data-target="#popup_produtos">Entregar</div>
								</div>
								<div class = "col-xs-12">
									<div type="button" class="btn botao_popup" data-toggle="modal" data-target="#popup_produtos" style="background-color:rgba(0,0,0,.3);">Voltar</div>
								</div>
								<div class = "col-xs-12">
									<div id="btn_cancelar_pedido" type="button" class="btn botao_popup" style="background-color:#f00;">Recusar Pedido</div>
								</div>
								<div class = "col-xs-12">
											<label for = "input_obs" id = "label_obs">Caso vá recusar o pedido,digite aqui a observação de cancelamento</label>
											<input id = "input_obs" class="txt_modal" placeholder="Digite aqui">											
								</div>
								

							</div>
						</div>					
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>

		
		
		
		
		
		
		
		
		
	</section>	
	
	<!-- Rodapé-->
    <footer>
		<!-- 		<div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; IFFood 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              
            </ul>
          </div>
        </div>
      </div> -->
    </footer>
	<!-- JS do Bootstrap (nao tirar daqui)-->
    <script type="text/javascript" src = "../../_bootstrap/js/bootstrap.min.js"></script>  
	</body>
</html>