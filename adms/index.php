<?php
	session_start();
	if (isset($_SESSION["user"]) && isset($_SESSION["pass"])) {
		header("location: panel/");
	}
?>
<!DOCTYPE html>
<html lang = "pt-br">
<head>
	<meta http-equiv = "X-UA-Compatible" content="IE-edge" /><!-- Frescura do Internet Explorer. ESTA TAG DEVE VIR ANTES DE TODAS OUTRAS COISAS-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login do Vendedor</title>

	<!-- Importando Estilo,Fontes,Scripts-->
	<link rel="stylesheet" href="../_bootstrap/css/bootstrap.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet"><!-- font-family: 'Exo 2', sans-serif; -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="../_css/style.css" />
	<link rel="stylesheet" type="text/css" href="../_css/nav.css" />
	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src = "../_script/admin.js"></script>


</head>
<body>


	<!-- Cabe�alho De Navega��o -->
	<header id="nav">
		<nav class="navbar navbar-default" id="container_nav">
			<div class = "container">
				<div class= "navbar-header" id="nav-div-button">
					<button id="nav-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barra-navegacao">
						<span class="sr-only">Alternar Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
					<a href="../index.php" class="navbar-brand" id="name-iffood">IFFood</a>
				</div>
				<div class = "collapse navbar-collapse" id = "barra-navegacao">
					<ul class = "nav navbar-nav navbar-right" id="lista-navegacao">
						<li id="pg_home"><i class="material-icons icon_op">&#xE8CC;</i><a href="../">Cardápio</a></li>
						<li id="pg_history"><i class="material-icons icon_op">&#xE8A6;</i><a href="../_elements/sobre.php">Sobre Nós</a></li>
						<li id="pg_contato"><i class="material-icons icon_op">&#xE0E1;</i><a href="../_elements/contato.php">Contato</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>


	<!-- Corpo do Site-->
	<section class="corpo" style = "padding-bottom:150px;">


		<!-- Aba de login -->
		<div class = "container">
			<div class = "row">
				<div class = "col-xs-10 col-xs-offset-1">
					<h1 style = "text-align:center;margin:50px 0px 50px 0px;">Login de Vendedor</h1>
				</div>

			</div>
			<div class = "row">
				<div class = "col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
					<div>
						<form id = "form_login" action = "panel/login.php" method = "post">
							<div class = "form-group" style = "margin-bottom:50px;">
								<label for = "input_nome_vendedor">Nome De Usuario</label>
								<input type = "text" class="form-control" name="user" id="input_nome_vendedor" placeholder = "Digite seu nome de usuario...">
							</div>
							<div class="form-group">
								<label for="input_senha_vendedor">Senha</label>
								<input type = "password" name="pass" class = "form-control" id = "input_senha_vendedor" placeholder = "Digite sua senha">
							</div>
							<button type="submit" class="btn" id = "bota_login" style = "margin-top:30px; background-color:#0f0;font-size:15px;font-weight:600;color:white;">Sign in</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section> 

	<!-- JS do Bootstrap (nao tirar daqui)-->
    <script type="text/javascript" src = "../_bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
