<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE-edge" />
	<!-- Frescura do Internet Explorer. ESTA TAG DEVE VIR ANTES DE TODAS OUTRAS COISAS-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>IFFood</title>
	<!-- Importando Estilo,Fontes,Scripts-->
	<link rel="stylesheet" href="_bootstrap/css/bootstrap.min.css" />
	<!-- Bootstrap-->
	<link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet">
	<!-- font-family: 'Exo 2', sans-serif; -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!--Futuramente,Link para Formatação de dispositivos com largura menor que 350px-->
	<link type="text/css" rel="stylesheet" href="_css/style.css" />
	<link rel="stylesheet" type="text/css" href="_css/nav.css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- JQuery-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<!-- Fonte de icones -->
	<link href="_script/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Exo+2:900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
	<!-- IFRAME HEADER -->
	<iframe src="_elements/header.html" width="100%" frameborder="0" height="66px" class="iframeHeader"></iframe>
	<!-- Avisso -->
	<div class="container-fluid">
		<div id="aviso" class="row" style="display:none;">
			<div class="col-xs-12 col-sm-7 col-sm-offset-2 col-lg-5 col-lg-offset-3">
				<div id="text_aviso">
					<p></p>
				</div>
			</div>
			<div class="col-xs-2 col-xs-offset-5 col-sm-1 col-sm-offset-0 ">
				<div id="div_icon_aviso">
					<i id="icon_aviso" class="material-icons"></i>
				</div>
			</div>

		</div>
	</div>

	<section class="corpo">
		<!-- Não Menu -->
		<div id="div_cardapio" class="container">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2 col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0">
					<h1 id="titulo_cardapio" class="ttl_card">Cardápio</h1>
					<!-- Titulo Cardápio -->
				</div>
			</div>
			<div id="cardapio" class="row">
				<div id="gambi" class="col-xs-12 col-sm-12 col-lg-12">
				</div>
			</div>
		</div>
		<!-- PopUp de finalização de compra -->
		<div id="popup" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 id="titulo_popup" class="modal-title">Informações Finais</h4>
					</div>
					<div class="modal-body">
						<div id="div_compra" class="container-fluid">
							<div class="row">
								<!-- valor total da compra -->
								<div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2">
									<div class="pre_form">
										<span id="label_compra">Total: </span>
										<input id="input_compra_final" form="form_compra" type="text" value="0" readonly />
									</div>
								</div>
								<!-- Input de nome -->
								<div class="col-xs-12 col-lg-12">
									<div id="div_nome_compra">
										<label style="font-size:20px;" for="txt_nome">Nome</label>
										<input id="txt_nome" class="txt_modal" type="text" placeholder="Campo Obrigatório">
									</div>
								</div>
								<!-- Local de Entrega -->
								<!-- predefinido -->
								<div class="col-xs-12 col-lg-12">
									<div id="div_select_local">
										<div>
											<label style="font-size:20px;" for="select_local">Local de Entrega</label>
										</div>
										<input id="checkbox_local" type="checkbox">
										<input id="input_local_entrega" class="txt_modal" placeholder="Selecione caso vá digitar o local">
										<h3 id="h3_checkbox" class="sub_text">Local de entrega específico?</h3>
										<select class="form-control" id="select_local">
											<!-- Values devem estar sem acento-->
										</select>
									</div>
								</div>
								<!-- carrinho -->
								<div class="col-xs-12 col-lg-12">
									<label id="label_carrinho"></label>
								</div>
								<!-- forma de pagamento -->
								<div class="col-xs-12">
									<div class="radio-inline" style="padding:0px;">
										<label class="radio-inline radiozin">
											<input id="radioPagamento_dinheiro" type="radio" name="radioPagamento" value="" checked>Dinheiro</label>
										<label class="radio-inline radiozin">
											<input id="radioPagamento_cartao" type="radio" name="radioPagamento" value="">Cartão</label>

										<div id="div_troco" style="margin-top:15px;">
											<label for="troco">Troco</label>
											<input id="input_troco" class="txt_modal" type="text" name="troco" value="" placeholder="Quantia disponível">
										</div>
									</div>
								</div>
								<!-- Botoes de confirmação e cancelamento -->
								<div class="col-xs-6  col-sm-6">
									<div class="pre_form">
										<button id="btn_compra_final" type="button" class="btn botao_popup">Finalizar</button>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6">
									<div class="pre_form">
										<button id="btn_cancelar_compra" type="button" class="btn botao_popup" data-toggle="modal" data-target="#popup" style="background-color:#f00;">Cancelar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Footer-->
	<iframe src="_elements/footer.html" width="100%" frameborder="0" height="325px" class="iframeFooter"></iframe>

	<section id="section_btn_compra">
		<div id="div_finalizacao">
			<div id="div_compra" class="container">
				<div class="row" style="margin-right: -8px;">
					<div id="bg_preco" class="col-xs-8 col-xs-offset-2">
						<div class="col-xs-12 col-sm-3 col-sm-offset-3 col-md-2-offset-4">
							<div id="label_compra">Total: </div>
						</div>
						<div class="col-xs-10 col-xs-offset-1 col-sm-3 col-sm-offset-0 col-md-2">
							<input id="input_compra" form="form_compra" type="text" value="R$ 0" readonly />
						</div>
					</div>
					<div id="bg_botao" class="col-xs-8  col-xs-offset-2 ">
						<div class="pre_form">
							<button id="btn_compra" type="button" class="btn botao_popup" data-toggle="modal" data-target="#popup"></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<script type="text/javascript" src="_script/main.js"></script>
	<!-- JS do Bootstrap (nao tirar daqui)-->
	<script type="text/javascript" src="_bootstrap/js/bootstrap.min.js"></script>
	<!-- <script>
		
		// var documentIframe= meuIframe.contentDocument || meuIframe.contentWindow.document; 
		// var botao = documentIframe.querySelector("btnCarrinho");
		// botao.addEventListener("click", function(){
		// 	var btnAbreModal = document.querySelector("#btn_compra");
		// 	btnAbreModal.click();
		// 	alert(meuIframe);
		// });

		$(function(){
			var meuIframe= document.querySelector(".iframeHeader");
			var documentIframe= meuIframe.contentDocument || meuIframe.contentWindow.document; 
			var btnCarrinho = documentIframe.getElementById('pg_carrinho');
			btnCarrinho.onclick=function(event)
			{
				var btnAbreModal = document.querySelector("#btn_compra");
				btnAbreModal.click();
			}
		});
	</script> -->
</body>
</html>