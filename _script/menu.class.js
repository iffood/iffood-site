function Menu(btn, mn, classIn, classOut) {
	var self = this;
	var animationend = "webkitAnimationEnd mozAnimationEnd animationend";
	var exit = $("<div id='exit'></div>");
	exit.css({"display":"none","position":"fixed","top":"0px","left":"0px","width": "100%","height":"100%","background-color":"rgba(0,0,0,0.5)","z-index":"1"});
	$("body").append(exit);// Add exit on body
	self.click = function() {
		self.open();
	};

	self.open = function() {
		$(mn).show().removeClass(classOut).addClass(classIn);
		exit.fadeIn();
	};

	self.close = function() {
		exit.fadeOut(200);
		$(mn).removeClass(classIn).addClass(classOut).on(animationend, function(){
			$(this).hide().off(animationend);
		});
	};

	$(btn).click(self.click);
	exit.click(self.close);
}