$(window).on("load",function(){

	var pedidosAceitos = [];	

	
	getLocais();
	seller_ping();
	criarListaPedidos();
	criarListaPedidosAceitados();
	getProdutos();
	setInterval(seller_ping,600000);
	setInterval(criarListaPedidos,10000);
	setInterval(criarListaPedidosAceitados,10000);	
	

//--------PRODUTOS---------------------------------------------------------------------------------------------	
	
	var produtos = [];
	
	function Produto(name,price,id){		
		
		//Atributos
		this.nome = name;
		this.preco = price;
		this.subtotal = 0;
		this.id = id;
	};
	
	//Pega os produtos do banco de dados e cria um objeto pra cada um no vetor produtos[]
	function getProdutos(){
		$.ajax({
			url: "../../_core/_controller/produtos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action": "getProdutos"				
			},
			success: function(resp){
				if(resp){					
					for(var i = 0; i < resp.length; i++){							
						produtos.push(new Produto(resp[i].nome,resp[i].preco,resp[i].id));						
					}
				}else{
				// Ocorreu algum erro.
					console.log(resp);
				}
			//...
			},
			error:function(resp){
				console.log(resp + "\n \n entrou no erro");
			}
		});
	}
	
	
//-------PEDIDOS-------------------------------------------------------------------------------------------------------------------------------//	
	
	var pedidos = [];
	
	
	function Pedido(id,cliente,local,complemento,estado,pagamento,troco){ //estado = status (status é uma palavra reservada)
	
		var thiz = this;		
		
		var id = id;	
		
		this.id = id;
		this.comida;
		this.cliente = cliente;
		this.local = local;
		this.complemento = complemento;		
		this.estado = estado;
		this.pagamento = pagamento;
		this.troco = troco;		
		
		
		this.div_principal = $("<div class = 'pedidao_pop col-xs-12 col-sm-6 col-sm-offset-0 col-md-3'></div>");//div do pedido		
		this.pedido = $("<div class = 'pedido'></div>");//pedido	
		
		
		this.div_info = $("<div class = 'col-xs-12'></div>");//div das informacao
		this.info = $("<div class = 'info'><div class = 'divizao col-xs-12'><div class = 'information col-xs-12'>Cliente </div><div class = 'value col-xs-12'>"+this.cliente+"</div></div><div class = 'divizao col-xs-12'><div class = 'information col-xs-12'>Local </div><div class = 'value col-xs-12'>"+this.local+"</div></div><div class = 'divizao col-xs-12'><div class = 'information col-xs-12'>Complemento </div><div class = 'value col-xs-12'>"+this.complemento+"</div></div></div>");//informacoes
		
		this.div_produtos = $("<div class = 'div_produto_pedido col-xs-10 col-xs-offset-1'></div>");
		this.btn_mostrar_produtos = $("<button type='button' class='show_produtos btn botao_popup' data-toggle='modal' data-target='#popup_produtos'>Ver Produtos</button>");
		this.btn_confirmar = $("<button type='button' class='show_produtos btn botao_popup' data-toggle='modal' data-target='#popup_produtos' style = 'display:none;font-size'>Rejeitar/Confirmar</button>");
			
		
		this.criarPedido = function(tipo_pedido){				
				
			$(this.div_info).append(this.info);
			$(this.div_produtos).append(this.btn_mostrar_produtos);
			$(this.div_produtos).append(this.btn_confirmar);
			
			$(this.pedido).append(this.div_info);			
			$(this.pedido).append(this.div_produtos);	
			
			$(this.div_principal).append(this.pedido);			
			
			tipo_pedido.append(this.div_principal);				
		}		
	
		this.btn_mostrar_produtos.on('click',function(){
			addPedidoInfoPopup();
			$("#btn_confirmar_entrega_produto").css("display","none");
			$("#btn_aceitar_produto").css("display","block");
		});			
	
		this.btn_confirmar.on('click',function(){
			addPedidoInfoPopup();
			$("#btn_confirmar_entrega_produto").css("display","block");
			$("#btn_aceitar_produto").css("display","none");
		});	
		
		function addPedidoInfoPopup(){
			$.ajax({
				url: "../../_core/_controller/pedidos.php",
				method: "POST",
				dataType: "json",
				data: {
					"action": "getItemsOfPedido",
					"id": id
				},
				success: function(resp){
					if(resp){						
						console.log(resp);
						
						$(".infos_popup").remove();
						
						var DOM_id = $("<div class='infos_popup' id='id'><div class = 'information'>id</div></div>");
						var id_value = $("<div id = 'value_id' class = 'value'></div>");
						
						var DOM_cliente = $("<div class='infos_popup' id='local'><div class = 'information'>cliente</div></div>");
						var cliente_value = $("<div id = 'value_cliente' class = 'value' ></div>");
						
						var DOM_local = $("<div class='infos_popup' id='complemento'><div class = 'information'>local</div></div>");
						var local_value = $("<div id = 'value_local' class = 'value'></div>");
						
						var DOM_complemento = $("<div class='infos_popup' id='cliente'><div class = 'information'>complemento</div></div>");
						var complemento_value = $("<div id = 'value_complemento' class = 'value'></div>");
						
						var DOM_produtos = $("<div class='infos_popup' id='produtos'><div class = 'information'>produtos</div></div>");
						var produtos_value = $("<div id = 'value_produtos' class = 'value'></div>");
						
						var DOM_preco = $("<div class='infos_popup' id='preco'><div class = 'information'>preco</div></div>");
						var preco_value = $("<div id = 'value_preco' class = 'value'></div>");		
						
						
						
						
						var DOM_pagamento = $("<div class='infos_popup' id='pagamento'><div class = 'information'>pagamento</div></div>");
						var pagamento_value = $("<div id = 'value_pagamento' class = 'value'></div>");
						
						var DOM_troco = $("<div class='infos_popup' id='troco'><div class = 'information'>troco</div></div>");
						var troco_value = $("<div id = 'value_troco' class = 'value'></div>");
						

						
						
						var price_total = 0;
						
						//configurando nome e quantia de trufas	
						for(var i = 0; i < resp.length; i++){							
							var quantia_produto = resp[i].quantidade;							
							//calculando preco							
							for(var z = 0; z < produtos.length; z++){																
								if(produtos[z].id == resp[i].produto){
									price_total = price_total + (quantia_produto*produtos[z].preco);									
								}
							}
							
							var nome_produto = "";									
							for(var x = 0; x < produtos.length; x++){
								if( (produtos[x].id) == resp[i].produto ){									
									nome_produto = produtos[x].nome;
									produtos_value.append(nome_produto + " : ");
									produtos_value.append(quantia_produto + "  ");	
								}
							}							
						}	
						
						preco_value.append(price_total);						
						cliente_value.append(cliente);
						local_value.append(local);
						complemento_value.append(complemento);
						id_value.append(id);						
						pagamento_value.append(pagamento);
						troco_value.append(troco);
						
						
						DOM_preco.append(preco_value);
						DOM_id.append(id_value);
						DOM_cliente.append(cliente_value);
						DOM_local.append(local_value);
						DOM_complemento.append(complemento_value);
						DOM_produtos.append(produtos_value);
						DOM_pagamento.append(pagamento_value);
						DOM_troco.append(troco_value);
						
						$('#info_pedido').append(DOM_id);
						$('#info_pedido').append(DOM_cliente);
						$('#info_pedido').append(DOM_complemento);
						$('#info_pedido').append(DOM_local);	
						$('#info_pedido').append(DOM_produtos);
						$('#info_pedido').append(DOM_preco);
						$('#info_pedido').append(DOM_pagamento);
						$('#info_pedido').append(DOM_troco);
					}else{					
						console.log(resp);
					}
				},
			});
		}
		
		this.tranform_butons = function(){
			this.btn_confirmar.css("display","block");
			this.btn_mostrar_produtos.css("display","none");
		};
	
	
	}
	
	
	
	//Aparecer pedidos na tela
	function criarListaPedidos(){		
		$.ajax({
			url: "../../_core/_controller/pedidos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action": "getPedidosWhere",
				"status": "aguardando"
			},
			success: function(resp){
				if(resp){	
					console.log(resp);
					$("div#pedidos_gerais .pedidao_pop").remove();
					pedidos = [];
					for(var i = 0; i < resp.length; i++){						
						pedidos.push(new Pedido(resp[i].id,resp[i].cliente,getLocal(resp[i].local),resp[i].complemento,resp[i].status,resp[i].payment,resp[i].troco));
						x = $("div#pedidos_gerais");
						pedidos[i].criarPedido(x);
					}									
				}else{
				// Ocorreu algum erro.
					console.log(resp);
				}
			//...
			},
			error:function(resp){
				console.log(resp + "\n \n entrou no erro");
			}
		});	
	}
	
	
	
	
//------lOCAIS-----------------------------------------------------------------------------------------------------------------------------------//
	
	var locais = [];
	
	function Local(name,id){
		
		//Atributos
		this.nome = name;
		this.id = id;
	};
	
	//Pegar locais do banco
	function getLocais(){
		$.ajax({
			url: "../../_core/_controller/locais.php",
			method: "POST",
			dataType: "json",
			data: {
				"action": "getLocais"				
			},
			success: function(resp){
				if(resp){					
					for(var i = 0; i < resp.length; i++){							
						locais.push(new Local(resp[i].nome,resp[i].id));					
						
					}
					
				}else{
				// Ocorreu algum erro.
					console.log(resp);
				}
			//...
			},
			error:function(resp){
				console.log(resp + "\n \n entrou no erro");
			}
		});
	}

	//Retorna o nome do local
	function getLocal(id){
		var local_name = "";
		for(var i = 0; i < locais.length; i++){
			if(id == locais[i].id){				
				local_name = locais[i].nome;
				return local_name;
			}			
		}
		return local_name;
	}

	
//-------PEDIDOS ACEITOS--------------------------------------------------------------------------------------------------------------------------//
	

	$("#btn_aceitar_produto").on('click',function(){
		
		var id_pedido_atual = parseInt($("#value_id").text());				
		
		console.log(id_pedido_atual);
		
		$.ajax({
			url: "../../_core/_controller/pedidos.php",
			method: "POST", 
			dataType: "json", 
			data: {
				"action": "aceitarPedido",
				"pedido": id_pedido_atual,
				"user_vendedor": user,
				"pass_vendedor": pass
			},
			success: function(resp){				
					$("#aviso_pessoal").css("display","none");
					console.log(pedidos.length);
					for(var i = 0; i < pedidos.length; i++){
						if(pedidos[i].id == id_pedido_atual){

							var aux = $("div#pedidos_pessoais");
							var cliente = $("div#info_pedido div#value_cliente").text();
							var local = $("div#info_pedido div#value_local").text();
							var complemento = $("div#info_pedido div#value_complemento").text();

							pedidosAceitos.push(new Pedido(id_pedido_atual,cliente,local,complemento,"aceitado"));
							var position = (pedidosAceitos.length)-1;

							pedidosAceitos[position].criarPedido(aux);
							pedidosAceitos[position].tranform_butons();
							criarListaPedidos();
							aviso("Pedido aceito com sucesso.",3000,"&#xE876;","#63FF63");
						}
					}
			},
			error: function(resp){
				console.log(resp["responseText"]);
			}
		});		
	});

	$("#btn_confirmar_entrega_produto").on('click',function(){
		
		var id_pedido_atual = parseInt($("#value_id").text());	

		$.ajax({
			url: "../../_core/_controller/pedidos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action": "confirmarPedido", 
				"id": id_pedido_atual,
				"user_vendedor": user,  
				"pass_vendedor": pass
			},
			success: function(resp) {
				if (resp) {	
					console.log(resp)
					aviso("confirmação de entrega feita com sucesso",3000,"&#xE876;","#63FF63");
				}else{					
				}				
			}
		});
	});

		
	$("#btn_cancelar_pedido").on('click',function(){
		//tenta pegar o valor do  observation. Caso nao der,avise o usuario		
		try{
			var observation = $("input#input_obs").val();
			if(!(observation)){
				aviso("O campo observação é obrigatório.",3000,"&#xE88F;","#FF4C48");
				$("input#input_obs").focus();
			}else{
				var id = parseInt($("div#value_id").text());
				console.log(id);
				console.log(observation);
				console.log(user);
				console.log(pass);
				$.ajax({
				    url: "../../_core/_controller/pedidos.php",
				    method: "POST",
				    dataType: "json",
				    data:{
					    "action": "setObservacao", 
						"id": id,
						"user": user,
						"pass": pass,
						"obs": observation						
					},
				    success:function(resp){ 
						if(resp){
							$.ajax({
							    url: "../../_core/_controller/pedidos.php",
							    method: "POST",
							    dataType: "json",
							    data:{
									"action":"rejeitarPedido", 
									"pedido": id,
									"user_vendedor": user,
									"pass_vendedor": pass
								},
								success: function(resp){ 
									if(resp){
										$('#popup_produtos').modal('toggle');
										aviso("Pedido cancelado com sucesso.",3000,"&#xE876;","#63FF63");
										
									}else{
										aviso("Erro ao rejeitar pedido.",3500,"&#xE888;","#FF4C48");
									}				 
								}
							});	
						}else{	
							aviso("Erro na comunicação: Conexão fraca, parâmetros ou caminhos errados. Consulte os desenvolvedores.",3500,"&#xE888;","#FF4C48");
						}					  
				    },
					error:function(resp){
						console.log(resp)						
						aviso("Erro ao definir observação.",3500,"&#xE888;","#FF4C48");
					}
				});
			}			
		}catch(e){
			console.log("nao tem nada no campo!");
			aviso("O campo observação é obrigatório.",3000,"&#xE88F;","#FF4C48");
			$("input#input_obs").focus();				
		}		
	});
	
	function criarListaPedidosAceitados(){
		$.ajax({
			url: "../../_core/_controller/pedidos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action": "getPedidosWhere",
				"status": "aceitado"
			},
		    success: function(resp){
				if(resp){
					console.log(resp);
					$("div#pedidos_pessoais .pedidao_pop").remove();
					pedidosAceitos = [];
					for(var i = 0; i < resp.length; i++){
						pedidosAceitos.push(new Pedido(resp[i].id, resp[i].cliente, getLocal(resp[i].local), resp[i].complemento, resp[i].status, resp[i].payment, resp[i].troco));
						x = $("div#pedidos_pessoais");
						pedidosAceitos[i].criarPedido(x);
						$("div#aviso_pessoal").css("display","none");
						pedidosAceitos[pedidosAceitos.length-1].tranform_butons();
						
						/*for( var i = 0; i < pedidos.length; i++ ){
							if(pedidosAceitos[pedidosAceitos.length-1] == pedidos[i]){
								pedidos[i].remove();								
							}					
						}*/
						
					}
				}else{
				// Ocorreu algum erro.
					console.log(resp);
				}
			//...
			},
			error:function(resp){
				console.log(resp + "\n \n entrou no erro");
			}
		});
	}	
	
	//texto para expor,tempo de duração do aviso e codigo do icone do material icons
	//&#xE876; para sucesso /  &#xE888; para erro / &#xE88F; para info / &#xE0CE; para conection
	//#63FF63 verde successo/  #FF4C48 vermelho erro // #FFE34E para amarelo info
	function aviso(texto,duration,icon,cor_fundo){		
		$("div#aviso div#text_aviso p").remove()		
		$("div#aviso #icon_aviso").remove();
		
		//colocando texto na div
		var DOM_texto = $("<p></p>");		
		DOM_texto.append(texto);
		$("div#aviso div#text_aviso").append(DOM_texto);
		
		//icone de sucesso ou erro
		var icone = $("<i id ='icon_aviso' class='material-icons'></i>");
		icone.append(icon);
		$("#div_icon_aviso").append(icone);
		
		$( "div#aviso" ).css("backgroundColor",cor_fundo);
		$( "div#aviso" ).focus().fadeIn("300");
		setTimeout(function(){			
			$( "div#aviso" ).fadeOut("3000");			
		}, duration);
		
	}
	
	
	function seller_ping(){
		$.ajax({
			url: "../../_core/_controller/vendedores.php", 
			method: "POST",
			dataType: "json", 
			data: { 
				"action": "ping", 
				"user": user,
				"pass": pass				
			},
			success:function(resp){ 
				if (resp) {
					aviso("Você está online. Usuários podem fazer pedidos devido a sua atividade.",3500,"&#xE876;","#63FF63");
				} else {
					aviso("Você está offline. Verifique sua conexão e recarregue a página.",3500,"&#xE0CE;","#FFE34E");					
				}				
			},
			error:function(resp){
				aviso("Algo de errado aconteceu. Recarregue a página ou contate os desenvolvedores.",3500,"&#xE888;","#FF4C48");				
			}
			
		});

	}
	
	
	
	
});