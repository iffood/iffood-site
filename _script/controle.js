$(window).on('load',function(){	
	//CLASSE DOS PRODUTOS
	
	var img_prefix = "../_img/_produtos/";	
	var produtos = [];	
	
	getProdutos();
	
	function Produto(name, price, imagem, id,quantia){		
		// ATRIBUTOS
		var self = this;
		
		
		this.nome = name;
		this.preco = price;
		this.img = imagem;//src da foto trufa		
		this.id = id;
		this.quantia = quantia;

		this.subtotal = 0;
		//OBJETOS DO DOM
		this.divisao = $("<div class = 'col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0 col-centralizada'></div>");		
		this.item = $("<div class='food'></div>");//item				
				
		this.thumbnail = $("<div class = 'div_img' style='background-image: url("+img_prefix+this.img+");'></div>");
	//	this.thumbnail = $("<div class = 'div_img'><img class = 'img-responsive' src = "+this.img+" alt = 'Imagem do produto'/></div>");
		this.nome_dom = $("<div class ='info_produto col-xs-10 col-xs-offset-0'>"+this.nome+"</div>");
		this.quantia_estoque = $("<div class = 'quantia_estoque'><div class ='info col-xs-8 col-xs-offset-0'>Estoque: </div><div class = 'value'>"+this.quantia+"</div></div>");
		this.input_quant = $("<input class = 'input_quant' type = 'number' placeholder = 'Digite a nova quantia'></input>");
		this.btn_edit = $("<div class = 'div_btn'><div style = 'padding-top:4px;'>Editar Estoque</div></div>");		

		//FUNÇÃO QUE ADICIONA PRODUTO NO CARDAPIO
		this.criarProduto = function(){
			
			$(this.item).append(this.thumbnail).append(this.nome_dom).append(this.quantia_estoque).append(this.input_quant).append(this.btn_edit);
			
			
			$(this.divisao).append(this.item);
			$("#row_trufas").append(this.divisao); //colando item no cardapio
		}
		
		$(this.btn_edit).on('click',function(){	
		
			var x = $(this).siblings(".input_quant").val();		//$(".input_quant").val();			
			if(parseInt(x) < 0){
				aviso("Valor Inválido.",3000,"&#xE888;","#FF4C48");
			}else{
				$('#popup_controle').modal('toggle');
				$('#info_pedido').empty();
				$('#info_dinheiro').empty();

				$('#info_pedido').append(self.nome);
				$('#info_dinheiro').append(parseInt(x));
			}
			
			
			
		});
	
	};
	
	function getProdutos(){
		$.ajax({
			url: "../_core/_controller/produtos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action":"getProdutos"
			},
			success: function(resp){				
				console.log(resp)
				if (resp) {					
					for (var i=0;i<resp.length;i++) {
						produtos.push(new Produto(resp[i].nome, resp[i].preco, resp[i].img, resp[i].id,resp[i].disponiveis));
						produtos[i].criarProduto();
					}
				} else {
					console.log("erro!");
				}
			},
			error:function(resp){
				console.log(resp);
			}		
		});
	}
	
	$("#btn_aceitar_produto").on("click",function(){
		var dinheiro = $('#info_dinheiro').text();
		var nome_trufa =  $('#info_pedido').text();
		var preco;
		var end;
		var img;
		var id;
		
		for( var i = 0; i < produtos.length; i++ ){
			if(produtos[i].nome == nome_trufa){
				id = produtos[i].id;
				preco = produtos[i].preco;
				img = produtos[i].img;
			}
		}
		console.log(nome_trufa,preco,img,dinheiro);
		$.ajax({	
		    url: "../_core/_controller/produtos.php",
		    method: "POST",
		    dataType: "json",
		    data:{
				  "action": "editProduto", 
				  "id": id,
				  "pdt": {
					"nome" : nome_trufa ,
					"preco": preco,
					"img":img,					
					"disponiveis":dinheiro
				  }, 
				 "user": "Tiago",
				 "pass": "123"
		    },
		    success: function(resp) {
			  if (resp) {
				console.log("funfou");
			  }else{
				console.log("nao funfou");			 
			  }			 
			},
			error:function(resp){
				console.log(resp);
			}
		});

		
	});
	
	$("#btn_cancelar_pedido").on("click",function(){
		$('#popup_controle').modal('toggle');
	});
	
	
	//&#xE876; para sucesso /  &#xE888; para erro / &#xE88F; para info
	//#63FF63 verde successo/  #FF4C48 vermelho erro/ #FFE34E para amarelo info
	//EX: aviso("É necessário escolher algum produto.",3000,"&#xE888;","#FF4C48");	
	//texto para expor,tempo de duração do aviso e codigo do icone do material icons
	function aviso(texto,duration,icon,cor_fundo){		
		$("div#aviso div#text_aviso p").remove()		
		$("div#aviso #icon_aviso").remove();
		
		//colocando texto na div
		var DOM_texto = $("<p></p>");		
		DOM_texto.append(texto);
		$("div#aviso div#text_aviso").append(DOM_texto);
		
		//icone de sucesso ou erro
		var icone = $("<i id ='icon_aviso' class='material-icons'></i>");
		icone.append(icon);
		$("#div_icon_aviso").append(icone);
		
		$( "div#aviso" ).css("backgroundColor",cor_fundo);
		$( "div#aviso" ).focus().fadeIn("300");
		setTimeout(function(){			
			$( "div#aviso" ).fadeOut("3000");			
		}, duration);
		
	}
	
	
});	