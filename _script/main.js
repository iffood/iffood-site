//EXECUTA QUANDO A PAGINA CARREGA
$(window).on('load',function(){

		var produtos = [];








	getProdutos();
	setInterval(getProdutos,40000);

	//PEGA O PREÇO TOTAL DA COMPRA
	function getValorTotal(){
		var total = 0;
		for (var i=0;i<produtos.length;i++) {
			total += produtos[i].subtotal;
		}
		return total;
	}

	//ATUALIZA O PREÇO TOTAL NO LABEL DO DOM
	function atualiza_total(){
		$("#input_compra").val("R$ "+ getValorTotal());
	}

	//FUNÇÃO QUE MOSTRA NO MODAL O PREÇO TOTAL E OS PRODUTOS À COMPRAR
	$("#btn_compra").click(function(){
		$("#input_compra_final").val("R$ " + getValorTotal());
		var div_carrinho = $("<div id = 'div_carrinho'></div>");
		$(div_carrinho).append(criarCarrinho());
		$("#label_carrinho").append(div_carrinho);
	});

	//FUNÇÃO QUE APAGA DO MODAL OS PRODUTOS À COMPRAR
	$("#btn_cancelar_compra").on("click",function(){
		$("#div_carrinho").remove();
	});

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------//

	//CLASSE DOS PRODUTOS
	function Produto(name, price,imagem, id,quantia){
		// ATRIBUTOS
		var self = this;

		this.quantia = quantia;
		this.nome = name;
		this.preco = price;
		this.img = imagem//src da foto trufa
		this.subtotal = 0;
		this.id = id;
		this.prefix = "_img/_produtos/";


		//OBJETOS DO DOM
		this.divisao = $("<div class = 'col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0 col-centralizada'></div>");
		this.item = $("<div class='food'></div>");//item
		this.cont_entrada = $("<div class = 'container-fluid container_food'></div>");//container
		this.row_entrada = $("<div class ='row'></div>");//linha
		this.div_btn_mais = $("<div class = 'col-xs-4 div_buton'></div>");//	div do botao de mais
		this.div_btn_menos = $("<div class='col-xs-4 div_buton'></div>"); // div botao de menos
		this.div_input = $("<div class='col-xs-4 input_buton'></div>");// div input
		this.btn_quantia_menos = $("<div class='botao btn_quantia_menos'><p>-</p></div>"); // Botão de menos
		this.btn_quantia_mais = $("<div class='botao btn_quantia_mais'><p>+</p></div>"); // Botão de mais
		this.input_quantidade = $("<input class='quantia' type='text' value='0' readonly/>");// input quantidade
		this.ttl_food = $("<h3 class='ttl_food'>"+this.nome+"</h3>"); // Nome do produto
		this.view_preco = $("<h4 class ='attr_preco'>R$ "+this.preco+"</h4>");//Preço do Produto
		// this.thumbnail = $("<div class = 'div_img'><img class = 'img-responsive' src = "+this.img+" alt = 'Imagem de Trufa'/></div>");
		this.thumbnail = $("<div class = 'div_img' style='background-image: url(\""+this.prefix+this.img+"\");'></div>");
		var divDaGambi = $('#gambi');


		//ATUALIZA O ATRIBUTO SUBTOTAL DO PRODUTO E ATUALIZA NO DOM
		this.atualiza_preco = function(){
			self.subtotal = parseInt(self.input_quantidade.val())*self.preco;
			atualiza_total();
		};


		//EVENTO QUE DIMINUI QUANTIA DE TRUFAS NO BOTÃO DE MENOS
		this.btn_quantia_menos.click(function(){
			let input = $(this).parents().siblings('.input_buton').children('.quantia');//pegando endereco do campo input
			if(parseInt(input.val())>0) {
				input.val(parseInt(input.val())-1);
			}
			self.atualiza_preco();
		});


		//FVENTO QUE ACRESCENTA TRUFAS NO BOTÃO DE MAIS
		this.btn_quantia_mais.click(function(){
			let input = $(this).parents().siblings('.input_buton').children('.quantia');//pegando endereco do campo input
			var quantia_a_pedir = parseInt(input.val());
			if(self.quantia > quantia_a_pedir){
				input.val(parseInt(input.val())+1);
				self.atualiza_preco();
			}else{
				aviso("A quantia selecionada ultrapassa o estoque.",3000,"&#xE88F;","#FFE34E");
			}
		});


		//FUNÇÃO QUE ADICIONA PRODUTO NO CARDAPIO
		this.criarProduto = function(){
			$(this.div_btn_mais).append(this.btn_quantia_mais);
			$(this.div_btn_menos).append(this.btn_quantia_menos);
			$(this.div_input).append(this.input_quantidade);
			$(this.row_entrada).append(this.div_btn_menos).append(this.div_input).append(this.div_btn_mais);
			$(this.cont_entrada).append(this.row_entrada);

			$(this.item).append(this.ttl_food).append(this.thumbnail).append(this.view_preco).append(this.cont_entrada);
			$(this.divisao).append(this.item);
			divDaGambi.append(this.divisao); //colando item no cardapio
		}
	};

	//CRIANDO LISTENERS DO BOTAO DE MENOS E MAIS
	$(".btn_quantia_menos").click(atualiza_total);
	$(".btn_quantia_mais").click(atualiza_total);


	//FUNCAO QUE MOSTRA E ESCONDE INPUT DE LOCALIZAÇÃO
	$("#checkbox_local").on("click",function(){
		var input = document.getElementById("checkbox_local");
		if(input.checked){
			$("#input_local_entrega").css("display", "inline");
			$("#h3_checkbox").css("display","none");
		}else{
			$("#input_local_entrega").css("display", "none");
			$("#h3_checkbox").css("display","inline");
		}
	});

	//FUNCAO QUE ESCONDE E MOSTRA TROCO
	$(".radiozin").on("click",function(){
		var input = document.getElementById("radioPagamento_dinheiro");
		if(input.checked){
			$("#div_troco").show(500);
		}else{
			$("#div_troco").hide(500);

		}
	});

//  --------------------------------------------------------------------------------------------------------------------------------------------------------------

	//CRIA OS PRODUTOS DO BANCO DE DADOS
	function getProdutos(){
		$.ajax({
			url: "_core/_controller/produtos.php",
			method: "POST",
			dataType: "json",
			data: {
				"action":"getProdutos"
			},
			success: function(resp){
				console.log("funfou");
				console.log(resp)
				if (resp) {
					for (var i=0;i<resp.length;i++) {
						produtos.push(new Produto(resp[i].nome, resp[i].preco, resp[i].img, resp[i].id,resp[i].disponiveis));
						produtos[i].criarProduto();
					}
				} else {
					console.log("erro!");
				}
			},
			error:function(resp){
				console.log(resp);
			}
		});
	}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------

	//CRIA SELECTS NO POPUP DE COMPRA
	$.ajax({
		url: "_core/_controller/locais.php", // URL
		method: "POST", // Método
		dataType: "json", // Tipo da resposta
		data: {"action": "getLocais"}, // Paramêtros
		success: function(resp) { // Função que é chamada quando a requisição é efetuada
			if(resp){
				console.log("requisição de locais do banco de dados efetuada corretamente");
				var select_local = $('#select_local');
				for(var i = 0; i < resp.length; i++){
					select_local.append("<option value = " + resp[i].id + ">" + resp[i].nome + "</option>");
				}
			}else{
				console.log("erro! Não conseguiu efetuar resquisição");
			}
		},
		error:function(resp){

			console.log("Erro ao requisitar locais");
			console.log(resp);
		}
	});

	//MOSTRA OS PRODUTOS À COMPRAR NO MODAL
	function criarCarrinho(){
		var produtos_carrinho = "";
		for(var  i = 0; i < produtos.length; i++ ){
			if(parseInt(produtos[i].subtotal) > 0){
				var produtos_carrinho = produtos_carrinho + String(quantiaComida(i)) + " " + produtos[i].nome;
				produtos_carrinho = produtos_carrinho + ", ";
			}

		}
		produtos_carrinho = produtos_carrinho.substr(0,(produtos_carrinho.length - 2));
		return produtos_carrinho;
	}


//  ---------------------------------------------------------------------------------------------------------------------------------------------------


//função que retorna objeto com id e quantidade de comida pedidas EX: {1:4 ,3:4 , 2:5}
		function comidaPedida(){
			var comida = {};
			for(var  i = 0; i < produtos.length; i++ ){
				if(parseInt(produtos[i].subtotal) > 0){
					comida[produtos[i].id] = quantiaComida(i);
				}
			}
			return comida;
		}


//-------------------------------------------------------------------------------------------------------------------------------------------------------------------


	//função que retorna quantia das trufas pedidas
	function quantiaComida(pos){
		var quantia = parseFloat(produtos[pos].subtotal)/parseFloat(produtos[pos].preco);
		return quantia;
	}


	//função de click no botão de finalizar a compra modal
	$("#btn_compra_final").click(function(){

		var nomeCliente;
		var id_local_entrega = 0;
		var complemento = "";
		var input = $("input#checkbox_local");
		var pagamento = "indefinido";
		var troco = "sem troco";

		try{
			id_local_entrega = $('#select_local').val()[0];
			complemento = $("input#input_local_entrega").val();
		}catch(e){
			try{
				if(input[0].checked){
					complemento = $("input#input_local_entrega").val();
					if(!(complemento)){
						aviso("É necessário a localização do cliente.",3000,"&#xE88F;","#FF4C48");
					}
				}else{
					aviso("É necessário a localização do cliente.",3000,"&#xE88F;","#FF4C48");
				}
			}catch(e){
				aviso("É necessário a localização do cliente.",3000,"&#xE88F;","#FF4C48");
			}
		}
		try{
			nomeCliente = $("input#txt_nome").val();
			if(!(nomeCliente)){
				aviso("É necessário o nome do cliente.",3000,"&#xE88F;","#FF4C48");
			}
		}catch(e){
			aviso("É necessário o nome do cliente.",3000,"&#xE88F;","#FF4C48");
		}
		try{
			var radiozin = $("#radioPagamento_dinheiro");
			if(radiozin[0].checked){
				pagamento = "dinheiro";
			}else{
				pagamento = "cartao";
			}
		}catch(e){
			console.log(e);
		}
		try{
			troco = $("input#input_troco").val();
		}catch(e){
			console.log(e);
		}


		var obj = {}
		var foodzao = {};
		foodzao = comidaPedida();
		if((nomeCliente != "") && ((id_local_entrega != 0) || (complemento != "")) && ((pagamento =="dinheiro") || (pagamento == "cartao")) && (foodzao != obj)){
			efetuarPedido(nomeCliente,id_local_entrega,complemento,pagamento,troco,foodzao);
		}

	});

	function efetuarPedido(cliente,local,complemento,pagamento,troco,produtos,id){

		$.ajax({
			url: "_core/_controller/vendedores.php",
			method: "POST",
			dataType: "json",
			data:{
				"action": "getOnVendedores"
			},
			success: function(resp){
				console.log(resp)
				if(resp != "0"){
					$.ajax({
						url: "_core/_controller/pedidos.php",
						method: "POST",
						dataType: "json",
						data:{
							"action": "pedir",
							"cliente": cliente,
							"local": local,
							"complemento": complemento,
							"payment": pagamento,
							"troco": troco,
							"produtos": produtos
						},
						success: function(resposta){
							if(resp){
								aviso("Pedido realizado com sucesso.",3000,"&#xE876;","#63FF63");
								$("#div_carrinho").remove(); //LIMPAR O CARRINHO
								$('#popup').modal('toggle');
							//    setCookie("pedido",id);
							//	setInterval(checarPedido,15000) para verificar se o pedido mudou de estado


							}else{
								aviso("Alguns do produtos selecionados acabaram de serem comprados. Por favor,recarregue a página e tente novamente.",3000,"&#xE888;","#FF4C48");
							}
						},
						error:function(resposta){
							aviso("É necessário escolher algum produto.",3000,"&#xE888;","#FF4C48");
						}
					});
				}else{
					aviso("Desculpe,não temos vendedores online no momento.",3000,"&#xE888;","#FF4C48");
				}
			}
		});

	}

	//texto para expor,tempo de duração do aviso e codigo do icone do material icons
	//&#xE876; para sucesso /  &#xE888; para erro / &#xE88F; para info
	//#63FF63 verde successo/  #FF4C48 vermelho erro/ #FFE34E para amarelo info
	//EX: aviso("É necessário escolher algum produto.",3000,"&#xE888;","#FF4C48");
	function aviso(texto,duration,icon,cor_fundo){
		$("div#aviso div#text_aviso p").remove()
		$("div#aviso #icon_aviso").remove();

		//colocando texto na div
		var DOM_texto = $("<p></p>");
		DOM_texto.append(texto);
		$("div#aviso div#text_aviso").append(DOM_texto);

		//icone de sucesso ou erro
		var icone = $("<i id ='icon_aviso' class='material-icons'></i>");
		icone.append(icon);
		$("#div_icon_aviso").append(icone);

		$( "div#aviso" ).css("backgroundColor",cor_fundo);
		$( "div#aviso" ).focus().fadeIn("300");
		setTimeout(function(){
			$( "div#aviso" ).fadeOut("3000");
		}, duration);

	}

	function checarVendedores(){
		$.ajax({
			url: "_core/_controller/vendedores.php",
			method: "POST",
			dataType: "json",
			data:{
				"action": "getOnVendedores"
			},
			success: function(resp){
				if(resp){
					console.log(resp)
					aviso("Desculpe,não temos vendedores online no momento.",3000,"&#xE88F;","#FFE34E");
				}
			}
		});
	}

	//função para verificar se o pedido esta caminhando
	function checarPedido(){
		try{
			getCookie("pedido");
		}catch(e){

		}
	}





	function setCookie(name, value) {
        var cookie = name + "=" + escape(value);
		document.cookie = cookie;
	}

	function getCookie(name) {
    var cookies = document.cookie;
    var prefix = name + "=";
    var begin = cookies.indexOf("; " + prefix);

    if (begin == -1) {

        begin = cookies.indexOf(prefix);

        if (begin != 0) {
            return null;
        }

    } else {
        begin += 2;
    }

    var end = cookies.indexOf(";", begin);

    if (end == -1) {
        end = cookies.length;
    }

    return unescape(cookies.substring(begin + prefix.length, end));
}

	function deleteCookie(name) {
       if (getCookie(name)) {
              document.cookie = name + "=" + ";";
       }
	}


















	var altura_botao = $("div#div_finalizacao").height();
	$("footer#footercardp").css("marginBottom",(altura_botao + 30));
});
