<?php

require_once "connectDB.php";

class Vendedores {

  function cadastrar($nome, $user, $pass) {
    global $db;
    # Cadastro de vendedor
    try {
      $query = $db->prepare("INSERT INTO Vendedores (nome, user, pass) VALUES (:nome, :user, :pass);");
      $query->bindValue(":nome", $nome, PDO::PARAM_STR);
      $query->bindValue(":user", $user, PDO::PARAM_STR);
      $query->bindValue(":pass", hash("SHA256", "vgh5Fvg67FK".$pass.$user), PDO::PARAM_STR);
      $query->execute();
      return true;
    } catch (PDOException $e) {
      return false;
    }
  }

  function mudarSenha($user, $pass, $passNew) {
    global $db;
    # Troca de senha
    if ($this->auth($user, $pass) !== false) {
      try {
        $query = $db->prepare("UPDATE Vendedores SET pass=:pass WHERE user=:user;");
        $query->bindValue(":user", $user, PDO::PARAM_STR);
        $query->bindValue(":pass", hash("SHA256", "vgh5Fvg67FK".$passNew.$user), PDO::PARAM_STR);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    }
  }

  function getOnVendedores() {
    global $db;
    # Retorna o número de vendedores online
    try {
      $query = $db->prepare("SELECT count(*) FROM Vendedores WHERE NOW()-lastActive < 60;");
      $query->execute();
      return intval($query->fetchAll(PDO::FETCH_ASSOC)[0]["count(*)"]);
    } catch (PDOException $e) {
      return false;
    }
  }

  function auth($user, $pass) {
    global $db;
    # Login do vendedor
    try {
      $password = hash("SHA256", "vgh5Fvg67FK".$pass.$user);
      $query = $db->prepare("SELECT id, nome FROM Vendedores WHERE user=:user and pass=:pass");
      $query->bindValue(":user", $user, PDO::PARAM_STR);
      $query->bindValue(":pass", $password, PDO::PARAM_STR);
      $query->execute();
      $resp = $query->fetchAll(PDO::FETCH_ASSOC);
      if (isset($resp[0])) {
        return ["id" => $resp[0]["id"], "nome" => $resp[0]["nome"]];
      } else {
        return false;
      }
    } catch (PDOException $e) {
      return false;
    }
  }

  function ping($user, $pass) {
    global $db;
    $auth = $this->auth($user, $pass);
    $id = $auth["id"];
    if ($auth !== false) {
      try {
        #Avisar servidor que esta vivo
        $query = $db->prepare("UPDATE Vendedores SET lastActive=:lastActive WHERE id=:id;");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->bindValue(":lastActive", date("y-m-d H:i:s"), PDO::PARAM_STR);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }
}
