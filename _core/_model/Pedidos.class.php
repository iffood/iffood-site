<?php

require_once "connectDB.php";
require_once "Vendedores.class.php";

class Pedidos {

  function getPedido($pedido) {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Pedidos WHERE id=:pedido;");
      $query->bindValue(":pedido", $pedido, PDO::PARAM_INT);
      $query->execute();
      return $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function getPedidos() {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Pedidos");
      $query->execute();
      return $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function getPedidosWhere($p) {
    global $db;
    # Consulta os pedidos.
    $status = isset($p["status"])?$p["status"]:"";
    $vendedor = isset($p["vendedor"])?$p["vendedor"]:"";
    if ($status=="" && $vendedor=="") {
      return false;
    }
    $params = ["status", "vendedor"];
    for ($i=0;$i<count($params);$i++) {
      if (${$params[$i]}!="") {
        $params[$i] = $params[$i]."=:".$params[$i];
      } else {
        array_splice($params, $i, 1);
        $i--;
      }
    }
    $params = implode(" AND ", $params);
    try {
      $query = $db->prepare("SELECT * FROM Pedidos WHERE $params;");
      if ($status != "") {
        $query->bindValue(":status", $status, PDO::PARAM_STR);
      }
      if ($vendedor != "") {
        $query->bindValue(":vendedor", $vendedor, PDO::PARAM_INT);
      }
      $query->execute();
      return $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function getItemsOfPedido($id) {
    global $db;
    # Retorna os itens de um determinado pedido.
    try {
      $query = $db->prepare("SELECT * FROM Itens WHERE pedido=:id;");
      $query->bindValue(":id", $id, PDO::PARAM_INT);
      $query->execute();
      return $query->fetchAll(PDO::FETCH_OBJ);
    } catch(PDOException $e) {
      return false;
    }
  }

  function checkEstoque($produtos) {
    global $db;
    # Verifica se os produtos estão disponiveis no estoque.
    $pdts = [];
    foreach ($produtos as $id => $quantidade) {
      $pdts[] = "id=$id";
    }
    $pdts = implode(" OR ", $pdts);
    $query = $db->prepare("SELECT id, disponiveis FROM Produtos WHERE $pdts;");
    $query->execute();
    $resp = $query->fetchAll(PDO::FETCH_ASSOC);
    $r = [];
    for ($i=0;$i<count($resp);$i++) {
      $r[$resp[$i]["id"]] = $resp[$i]["disponiveis"];
    }
    foreach ($produtos as $id => $quantidade) {
      if ($r[$id] < $quantidade) {
        return false;
      }
    }
    return true;
  }

  function pedir($cliente, $local, $complemento, $payment, $troco, $produtos) {
    global $db;
    # Pedir produto
    if ($this->checkEstoque($produtos)) {
      try {
        # Faz o pedido
        $query = $db->prepare("INSERT INTO Pedidos (cliente, local, complemento, payment, troco, data, status) VALUES (:cliente, :local, :complemento, :payment, :troco, NOW(), :status);");
        $query->bindValue(":cliente", trim($cliente), PDO::PARAM_STR);
        if ($local=="") {
          $query->bindValue(":local", null, PDO::PARAM_NULL);
        } else {
          $query->bindValue(":local", $local, PDO::PARAM_INT);
        }
        if ($complemento=="") {
          $query->bindValue(":complemento", null, PDO::PARAM_NULL);
        } else {
          $query->bindValue(":complemento", $complemento, PDO::PARAM_STR);
        }
        $query->bindValue(":payment", $payment, PDO::PARAM_STR);
        if ($troco=="") {
          $query->bindValue(":troco", null, PDO::PARAM_NULL);
        } else {
          $query->bindValue(":troco", $troco, PDO::PARAM_STR);
        }
        $query->bindValue(":status", "aguardando", PDO::PARAM_STR);
        $query->execute();
        $idPedido = intval($db->lastInsertId());
        # Liga os produtos ao pedido
        $itens = "";
        foreach ($produtos as $id => $quantidade) {
          $itens .= "(:produto$id, :pedido, :quantidade$id),";
        }
        $itens = substr($itens, 0, -1);
        $query = $db->prepare("INSERT INTO Itens (produto, pedido, quantidade) VALUES $itens;");
        $query->bindValue(":pedido", $idPedido, PDO::PARAM_INT);
        foreach ($produtos as $id => $quantidade) {
          $query->bindValue(":produto$id", $id, PDO::PARAM_INT);
          $query->bindValue(":quantidade$id", $quantidade, PDO::PARAM_INT);
        }
        $query->execute();
        # Ajustar estoque
        $query = $db->prepare("SELECT produto, quantidade FROM Itens WHERE pedido=:id;");
        $query->bindValue(":id", $idPedido, PDO::PARAM_INT);
        $query->execute();
        $produtos = $query->fetchAll(PDO::FETCH_ASSOC);
        for($i=0;$i<count($produtos);$i++) {
          $query = $db->prepare("UPDATE Produtos SET disponiveis = disponiveis - :vendidos WHERE id=:id;");
          $query->bindValue(":id", $produtos[$i]["produto"], PDO::PARAM_INT);
          $query->bindValue(":vendidos", $produtos[$i]["quantidade"], PDO::PARAM_INT);
          $query->execute();
        }
        return $idPedido;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function setAvaliacao($id, $avaliacao, $msg) {
    global $db;
    # Define a avaliação do cliente.
    try {
      $query = $db->prepare("UPDATE Pedidos SET avaliacao=:avaliacao, mensagem=:msg WHERE id=:id;");
      $query->bindValue(":id", $id, PDO::PARAM_INT);
      $query->bindValue(":avaliacao", $avaliacao, PDO::PARAM_STR);
      $query->bindValue(":msg", $msg, PDO::PARAM_STR);
      $query->execute();
      return true;
    } catch (PDOException $e) {
      return false;
    }
  }

  function setObservacao($id, $user, $pass, $obs) {
    global $db;
    # Define a observação do vendedor.
    $v = new Vendedores();
    if ($v->auth($user, $pass) !== false) {
      try {
        $query = $db->prepare("UPDATE Pedidos SET observacao=:observacao;");
        $query->bindValue(":observacao", $obs, PDO::PARAM_STR);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function confirmarPedido($id, $user_vendedor, $pass_vendedor) {
    global $db;
    $v = new Vendedores();
    $vendedor = $v->auth($user_vendedor, $pass_vendedor);
    if ($vendedor !== false) {
      try {
        $query = $db->prepare("SELECT id FROM Pedidos WHERE id=:id AND vendedor=:vendedor;");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->bindValue(":vendedor", $vendedor["id"], PDO::PARAM_INT);
        $query->execute();
        if($query->fetchAll(PDO::FETCH_ASSOC) == array()) {
          return false;
        }
        # Confirma a entrega do pedido
        $query = $db->prepare("UPDATE Pedidos SET status=:status WHERE id=:id;");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->bindValue(":status", "entregado", PDO::PARAM_STR);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function cancelarPedido($id) {
    global $db;
    # Cancelar Pedidos
    try {
      $query = $db->prepare("UPDATE Pedidos SET status=:status WHERE id=:id;");
      $query->bindValue(":id", $id, PDO::PARAM_INT);
      $query->bindValue(":status", "cancelado", PDO::PARAM_STR);
      $query->execute();
      # Ajustar estoque
      $query = $db->prepare("SELECT produto, quantidade FROM Itens WHERE pedido=:id;");
      $query->bindValue(":id", $id, PDO::PARAM_INT);
      $query->execute();
      $produtos = $query->fetchAll(PDO::FETCH_ASSOC);
      for($i=0;$i<count($produtos);$i++) {
        $query = $db->prepare("UPDATE Produtos SET disponiveis = disponiveis + :vendidos WHERE id=:id;");
        $query->bindValue(":id", $produtos[$i]["produto"], PDO::PARAM_INT);
        $query->bindValue(":vendidos", $produtos[$i]["quantidade"], PDO::PARAM_INT);
        $query->execute();
      }
      return true;
    } catch (PDOException $e) {
      return false;
    }
  }

  function rejeitarPedido($pedido, $user_vendedor, $pass_vendedor) {
    global $db;
    # Recusar Pedidos
    $query = $db->prepare("SELECT id FROM Pedidos WHERE id=:id AND observacao IS NOT NULL;");
    $query->bindValue(":id", $pedido, PDO::PARAM_INT);
    $query->execute();
    if (count($query->fetchAll(PDO::FETCH_ASSOC)) > 0) {
      $v = new Vendedores();
      $auth = $v->auth($user_vendedor, $pass_vendedor);
      if($auth !== false) {
        $vendedor = $auth["id"];
        try {
          $query = $db->prepare("UPDATE Pedidos SET vendedor=:vendedor, status=:status WHERE id=:pedido;");
          $query->bindValue(":pedido", $pedido, PDO::PARAM_INT);
          $query->bindValue(":vendedor", $vendedor, PDO::PARAM_INT);
          $query->bindValue(":status", "recusado", PDO::PARAM_STR);
          $query->execute();
          # Ajustar estoque
          $query = $db->prepare("SELECT produto, quantidade FROM Itens WHERE pedido=:id;");
          $query->bindValue(":id", $pedido, PDO::PARAM_INT);
          $query->execute();
          $produtos = $query->fetchAll(PDO::FETCH_ASSOC);
          for($i=0;$i<count($produtos);$i++) {
            $query = $db->prepare("UPDATE Produtos SET disponiveis = disponiveis + :vendidos WHERE id=:id;");
            $query->bindValue(":id", $produtos[$i]["produto"], PDO::PARAM_INT);
            $query->bindValue(":vendidos", $produtos[$i]["quantidade"], PDO::PARAM_INT);
            $query->execute();
          }
          return true;
        } catch (PDOException $e) {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  function aceitarPedido($pedido, $user_vendedor, $pass_vendedor) {
    global $db;
    $v = new Vendedores();
    $auth = $v->auth($user_vendedor, $pass_vendedor);
    if($auth !== false) {
      $query = $db->prepare("SELECT id FROM Pedidos WHERE id=:id AND vendedor IS NULL;");
      $query->bindValue(":id", $pedido, PDO::PARAM_INT);
      $query->execute();
      if($query->fetchAll(PDO::FETCH_ASSOC) == array()) {
        return false;
      }
      $vendedor = $auth["id"];
      # Aceitar Pedidos
      try {
        $query = $db->prepare("UPDATE Pedidos SET vendedor=:vendedor, status=:status WHERE id=:pedido;");
        $query->bindValue(":pedido", $pedido, PDO::PARAM_INT);
        $query->bindValue(":vendedor", $vendedor, PDO::PARAM_INT);
        $query->bindValue(":status", "aceitado", PDO::PARAM_STR);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }
}
