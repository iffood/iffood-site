<?php

require_once "connectDB.php";

class Locais {
  private $cacheLocais;

  function getCacheLocais() {
    return $this->cacheLocais;
  }

  function getLocais() {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Locais;");
      $query->execute();
      return $this->cacheLocais = $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function addLocal($nome) {
    global $db;
    # Inserir dados no DB
    try {
      $query = $db->prepare("INSERT INTO Locais (nome) VALUES (:nome);");
      $query->bindValue(":nome", $nome, PDO::PARAM_STR);
      $query->execute();
      return true;
    } catch (PDOException $e) {
      return false;
    }
  }
}
