<?php

require_once "connectDB.php";
require_once "Vendedores.class.php";

class Produtos {

  private $cacheProdutos;

  function getCacheProdutos() {
    return $this->cacheProdutos;
  }

  function getProduto($id) {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Produtos WHERE id=:id;");
      $query->bindValue(":id", $id, PDO::PARAM_INT);
      $query->execute();
      return $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function delProduto($id, $user, $pass) {
    global $db;
    $v = new Vendedores();
    if ($v->auth($user, $pass) !== false) {
      # Deletando produto
      try {
        $query = $db->prepare("DELETE FROM Produtos WHERE id=:id;");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function getProdutos() {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Produtos WHERE disponiveis > 0;");
      $query->execute();
      return $this->cacheProdutos = $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function getAllProdutos() {
    global $db;
    # Consulta o DB
    try {
      $query = $db->prepare("SELECT * FROM Produtos;");
      $query->execute();
      return $this->cacheProdutos = $query->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return false;
    }
  }

  function setProduto($pdt, $user, $pass) {
    global $db;
    $v = new Vendedores();
    if ($v->auth($user, $pass) !== false) {
      # Inserindo dados no DB
      try {
        $query = $db->prepare("INSERT INTO Produtos (nome, preco, img, disponiveis) VALUES (:nome, :preco, :img, :disponiveis)");
        $query->bindValue(":nome", $pdt["nome"], PDO::PARAM_STR);
        $query->bindValue(":preco", $pdt["preco"], PDO::PARAM_STR);
        $query->bindValue(":img", $pdt["img"], PDO::PARAM_STR);
        $query->bindValue(":disponiveis", $pdt["disponiveis"], PDO::PARAM_INT);
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function editProduto($id, $pdt, $user, $pass) {
    global $db;
    $v = new Vendedores();
    if ($v->auth($user, $pass) !== false) {
      # Atualizando dados do DB
      $campos = ["nome", "preco", "img", "disponiveis"];
      $q = []; # String da query dinamica
      for ($i=0;$i<count($pdt);$i++) {
        if (isset($pdt[$campos[$i]])) {
          $q[] = $campos[$i]."=:".$campos[$i];
        }
      }
      $q = implode(", ", $q);
      try {
        $query = $db->prepare("UPDATE Produtos SET $q WHERE id=:id");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        for ($i=0;$i<count($pdt);$i++) {
          if (isset($pdt[$campos[$i]])) {
            $query->bindValue(":".$campos[$i], $pdt[$campos[$i]], PDO::PARAM_STR);
          }
        }
        $query->execute();
        return true;
      } catch (PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }

  function editEstoque($id, $disponiveis, $user, $pass) {
    global $db;
    # Atualizar estoque
    $v = new Vendedores();
    if ($v->auth($user, $pass) !== false) {
      try {
        $query = $db->prepare("UPDATE Produtos SET disponiveis=:disponiveis WHERE id=:id;");
        $query->bindValue(":id", $id, PDO::PARAM_INT);
        $query->bindValue(":disponiveis", $disponiveis, PDO::PARAM_INT);
        $query->execute();
        return true;
      } catch(PDOException $e) {
        return false;
      }
    } else {
      return false;
    }
  }
}
