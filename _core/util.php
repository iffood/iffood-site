<?php

function returnModule($args, $resp) {
  if ((isset($args["json"]) && $args["json"]) || !isset($args["json"])) {
    echo json_encode($resp);
  } else {
    return $resp;
  }
}
