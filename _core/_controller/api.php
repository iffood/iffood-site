<?php

require_once "locais.php";
require_once "pedidos.php";
require_once "produtos.php";
require_once "vendedores.php";

if (isset($_POST["reqs"])) {
  $reqs = $_POST["reqs"];
  $res = [];
  foreach ($reqs as $key => $req) {
    $req["json"] = false;
    $res[$key] = getResp($req);
  }
  echo json_encode($res);
}

function getResp($req) {
  if ($r = actionLocais($req)) {
    return $r;
  } else if ($r = actionPedidos($req)) {
    return $r;
  } elseif ($r = actionProdutos($req)) {
    return $r;
  } elseif ($r = actionVendedores($req)) {
    return $r;
  }
}
