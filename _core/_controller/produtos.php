<?php

require_once "../_model/Produtos.class.php";
require_once "../util.php";

function actionProdutos ($args) {
  $action = $args["action"];
  switch ($action) {
    case "getProdutos":
      return getProdutos($args);
    break;

    case "getAllProdutos":
      return getAllProdutos($args);
    break;

    case "getProduto":
      return getProduto($args);
    break;

    case "setProduto":
      return setProduto($args);
    break;

    case "delProduto":
      return delProduto($args);
    break;

    case "editProduto":
      return editProduto($args);
    break;

    case "editEstoque":
      return editEstoque($args);
    break;
  }
}

function getProdutos($args) {
  # ativa modulo de obter produtos
  $p = new Produtos();
  return returnModule($args, $p->getProdutos());
}

function getAllProdutos($args) {
  # ativa modulo de obter produtos
  $p = new Produtos();
  return returnModule($args, $p->getAllProdutos());
}

function getProduto($args) {
  # ativa o modulo de obter produto
    if (isset($args["pdt"])) {
      $p = new Produtos();
      $pdt = $args["pdt"];
      return returnModule($args, $p->getProduto($pdt));
    }
}

function delProduto($args) {
  # ativa o modulo de obter produto
    if (isset($args["id"]) && isset($args["user"]) && isset($args["pass"])) {
      $p = new Produtos();
      $id = $args["id"];
      $user = $args["user"];
      $pass = $args["pass"];
      return returnModule($args, $p->delProduto($id, $user, $pass));
    }
}

function setProduto($args) {
  # ativa o modulo que cadastra produtos
  if ((isset($args["pdt"]) || (isset($args["nome"]) && isset($args["preco"]) && isset($args["img"]) && isset($args["disponiveis"]))) && isset($args["user"]) && isset($args["pass"])){
    if (isset($args["pdt"])) {
      $pdt = $args["pdt"];
    } else {
      $pdt = [];
      $pdt["nome"] = $args["nome"];
      $pdt["preco"] = $args["preco"];
      $pdt["img"] = $args["img"];
      $pdt["disponiveis"] = $args["disponiveis"];
    }
    $user = $args["user"];
    $pass = $args["pass"];
    $p = new Produtos();
    return returnModule($args, $p->setProduto($pdt, $user, $pass));
  }
}

function editProduto($args) {
  # ativa o modulo que edita um produto
  if ((isset($args["pdt"]) || (isset($args["nome"]) && isset($args["preco"]) && isset($args["img"]) && isset($args["disponiveis"]))) && isset($args["id"]) && isset($args["user"]) && isset($args["pass"])){
    if (isset($args["pdt"])) {
      $pdt = $args["pdt"];
    } else {
      $pdt = [];
      $pdt["nome"] = $args["nome"];
      $pdt["preco"] = $args["preco"];
      $pdt["img"] = $args["img"];
      $pdt["disponiveis"] = $args["disponiveis"];
    }
    $id = $args["id"];
    $user = $args["user"];
    $pass = $args["pass"];
    $p = new Produtos();
    return returnModule($args, $p->editProduto($id, $pdt, $user, $pass));
  }
}

function editEstoque($args) {
  # ativa o modulo que edita um produto
  if (isset($args["disponiveis"]) && isset($args["id"]) && isset($args["user"]) && isset($args["pass"])){
    $disponiveis = $args["disponiveis"];
    $id = $args["id"];
    $user = $args["user"];
    $pass = $args["pass"];
    $p = new Produtos();
    return returnModule($args, $p->editEstoque($id, $disponiveis, $user, $pass));
  }
}

if (isset($_POST["action"])) {
  actionProdutos($_POST);
}
