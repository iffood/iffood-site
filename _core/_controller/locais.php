<?php

require_once "../_model/Locais.class.php";
require_once "../util.php";

function actionLocais($args) {
  $action = $args["action"];
  switch ($action){
    case "getLocais":
      return getLocais($args);
    break;

    case "addLocal":
      return addLocal($args);
    break;
  }
}

function getLocais($args) {
  $l = new Locais();
  return returnModule($args, $l->getLocais());
}

function addLocal($args) {
  if(isset($args["nome"])) {
    $nome = $args["nome"];
    $l = new Locais();
    return returnModule($args, $l->addLocal($nome));
  }
}

if (isset($_POST["action"])) {
  actionLocais($_POST);
}
