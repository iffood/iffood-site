<?php

require_once "../_model/Pedidos.class.php";
require_once "../util.php";

function actionPedidos($args) {
  $action = $args["action"];
  switch ($action){
    case "getPedido":
      return getPedido($args);
    break;

    case "getPedidos":
      return getPedidos($args);
    break;

    case "getPedidosWhere":
      return getPedidosWhere($args);
    break;

    case "getItemsOfPedido":
      return getItemsOfPedido($args);
    break;

    case "pedir":
      return pedir($args);
    break;

    case "setAvaliacao":
      return setAvaliacao($args);
    break;

    case "setObservacao":
      return setObservacao($args);
    break;

    case "confirmarPedido":
      return confirmarPedido($args);
    break;

    case "cancelarPedido":
      return cancelarPedido($args);
    break;

    case "rejeitarPedido":
      return rejeitarPedido($args);
    break;

    case "aceitarPedido":
     return aceitarPedido($args);
    break;
  }
}

function getPedido($args) {
  if (isset($args["pedido"])) {
    $pedido = $args["pedido"];
    $pe = new Pedidos();
    return returnModule($args, $pe->getPedido($pedido));
  }
}

function getPedidos($args) {
  $pe = new Pedidos();
  return returnModule($args, $pe->getPedidos());
}

function getPedidosWhere($args) {
  if (isset($args["status"]) || isset($args["vendedor"])) {
    $params = [];
    $params["status"] = isset($args["status"])?$args["status"]:"";
    $params["vendedor"] = isset($args["vendedor"])?$args["vendedor"]:"";
    $pe = new Pedidos();
    return returnModule($args, $pe->getPedidosWhere($params));
  }
}

function getItemsOfPedido($args) {
  if (isset($args["id"])) {
    $id = $args["id"];
    $pe = new Pedidos();
    return returnModule($args, $pe->getItemsOfPedido($id));
  }
}

function pedir($args) {
  if((isset($args["cliente"]) && isset($args["local"]) && isset($args["complemento"]) && isset($args["payment"]) && isset($args["troco"])) && (isset($args["produtos"]) || (isset($args["produto1"]) && isset($args["quantidade1"])))){
    $cliente = $args["cliente"];
    $local = $args["local"];
    $complemento = $args["complemento"];
    $payment = $args["payment"];
    $troco = $args["troco"];
    $produtos = [];
    if (isset($args["produtos"])) {
      $produtos = $args["produtos"];
    } else {
      $i = 1;
      while(isset($args["produto$i"]) && isset($args["quantidade$i"])) {
        $produtos[$args["produto$i"]] = $args["quantidade$i"];
        $i++;
      }
    }
    $pe = new Pedidos();
    return returnModule($args, $pe->pedir($cliente, $local, $complemento, $payment, $troco, $produtos));
  }
}

function setAvaliacao($args) {
  # Definir a avaliação
  if (isset($args["id"]) && isset($args["avaliacao"]) && isset($args["msg"])) {
    $id = $args["id"];
    $avaliacao = $args["avaliacao"];
    $msg = $args["msg"];
    $pe = new Pedidos();
    return returnModule($args, $pe->setAvaliacao($id, $avaliacao, $msg));
  }
}

function setObservacao($args) {
  # Definir a observação
  if (isset($args["id"]) && isset($args["user"]) && isset($args["pass"]) && isset($args["obs"])) {
    $id = $args["id"];
    $user = $args["user"];
    $pass = $args["pass"];
    $obs = $args["obs"];
    $pe = new Pedidos();
    return returnModule($args, $pe->setObservacao($id, $user, $pass, $obs));
  }
}

function confirmarPedido($args) {
  if(isset($args["id"]) && isset($args["user_vendedor"]) && isset($args["pass_vendedor"])){
   $id = $args["id"];
   $user_vendedor = $args["user_vendedor"];
   $pass_vendedor = $args["pass_vendedor"];
   $pe = new Pedidos();
   return returnModule($args, $pe->confirmarPedido($id, $user_vendedor, $pass_vendedor));
  }
}

function cancelarPedido($args) {
  if(isset($args["id"])){
   $id = $args["id"];
   $pe = new Pedidos();
   return returnModule($args, $pe->cancelarPedido($id));
  }
}

function rejeitarPedido($args) {
  if(isset($args["pedido"]) && isset($args["user_vendedor"]) && isset($args["pass_vendedor"])){
   $pedido = $args["pedido"];
   $user_vendedor = $args["user_vendedor"];
   $pass_vendedor = $args["pass_vendedor"];
   $pe = new Pedidos();
   return returnModule($args, $pe->rejeitarPedido($pedido, $user_vendedor, $pass_vendedor));
  }
}

function aceitarPedido($args) {
  if(isset($args["pedido"]) && isset($args["user_vendedor"]) && isset($args["pass_vendedor"])){
   $pedido = $args["pedido"];
   $user_vendedor = $args["user_vendedor"];
   $pass_vendedor = $args["pass_vendedor"];
   $pe = new Pedidos();
   return returnModule($args, $pe->aceitarPedido($pedido, $user_vendedor, $pass_vendedor));
  }
}

if (isset($_POST["action"])) {
  actionPedidos($_POST);
}
