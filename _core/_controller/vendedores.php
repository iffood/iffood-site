<?php

require_once "../_model/Vendedores.class.php";
require_once "../util.php";

function actionVendedores($args) {
  $action = $args["action"];
  switch ($action) {

    # Está função estará desabilitada, pois por enquanto os cadastros serão feitos manualmente pelo Tiago.
    // case 'cadastrar':
    //   if (isset($_POST["nome"]) && isset($_POST["user"]) && isset($_POST["pass"])) {
    //     $v = new Vendedores();
    //     $v->cadastrar($_POST["nome"], $_POST["user"], $_POST["pass"]);
    //   }
    //   break;

    # Retorna o número de vendedores online
    case "getOnVendedores":
      return getOnVendedores($args);
    break;

    # Muda a senha do vendedor
    case 'mudarSenha':
      return mudarSenha($args);
    break;

    # Avisa o servidor que o vendedor está online
    case 'ping':
      return ping($args);
    break;

    # Logar no sistema
    case "logar":
      return logar($args);
    break;
  }
}

function getOnVendedores($args) {
  $v = new Vendedores();
  return returnModule($args, $v->getOnVendedores());
}

function mudarSenha($args) {
  if (isset($args["user"]) && isset($args["pass"]) && isset($args["newPass"])) {
    $v = new Vendedores();
    return returnModule($args, $v->mudarSenha($args["user"], $args["pass"], $args["newPass"]));
  }
}

function ping($args) {
  if (isset($args["user"]) && isset($args["pass"])) {
    $user = $args["user"];
    $pass = $args["pass"];
    $v = new Vendedores();
    return returnModule($args, $v->ping($user, $pass));
  }
}

function logar($args) {
  if (isset($args["user"]) && isset($args["pass"])) {
    $user = $args["user"];
    $pass = $args["pass"];
    $v = new Vendedores();
    return returnModule($args, $v->auth($user, $pass));
  }
}

if (isset($_POST["action"])) {
  actionVendedores($_POST);
}
