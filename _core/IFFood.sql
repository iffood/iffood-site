-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 31-Mar-2018 às 14:53
-- Versão do servidor: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `IFFood`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `Itens`
--

CREATE TABLE `Itens` (
  `produto` int(11) NOT NULL,
  `pedido` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Itens`
--

INSERT INTO `Itens` (`produto`, `pedido`, `quantidade`) VALUES
(1, 18, 5),
(1, 31, 1),
(1, 33, 3),
(1, 34, 1),
(1, 35, 1),
(1, 36, 1),
(1, 37, 1),
(1, 39, 3),
(1, 40, 3),
(1, 41, 2),
(1, 42, 3),
(1, 43, 3),
(1, 44, 2),
(1, 45, 3),
(1, 47, 2),
(1, 49, 3),
(1, 51, 1),
(1, 63, 2),
(2, 29, 3),
(2, 32, 1),
(2, 33, 10),
(2, 34, 1),
(2, 38, 10),
(2, 39, 10),
(2, 40, 10),
(2, 42, 10),
(2, 43, 5),
(2, 44, 2),
(2, 45, 3),
(2, 46, 1),
(2, 47, 2),
(2, 49, 10),
(2, 63, 2),
(2, 65, 2),
(3, 47, 2),
(3, 49, 23),
(3, 52, 3),
(3, 53, 2),
(3, 63, 2),
(3, 65, 1),
(4, 47, 2),
(4, 49, 5),
(4, 52, 2),
(4, 53, 2),
(5, 47, 2),
(5, 49, 15),
(5, 54, 1),
(5, 57, 2),
(5, 61, 1),
(6, 47, 2),
(6, 49, 2),
(6, 54, 2),
(6, 57, 2),
(6, 61, 1),
(7, 49, 20),
(7, 56, 1),
(7, 59, 1),
(7, 61, 1),
(7, 62, 1),
(8, 49, 16),
(8, 55, 1),
(8, 56, 1),
(8, 58, 2),
(8, 62, 1),
(8, 64, 1),
(9, 49, 4),
(9, 50, 1),
(9, 51, 1),
(9, 55, 2),
(9, 56, 1),
(9, 58, 2),
(9, 60, 1),
(9, 62, 2),
(9, 64, 1),
(10, 48, 1),
(10, 49, 1),
(10, 62, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Locais`
--

CREATE TABLE `Locais` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Locais`
--

INSERT INTO `Locais` (`id`, `nome`) VALUES
(3, 'Biblioteca'),
(1, 'Cantina'),
(4, 'Fornão'),
(2, 'Piscina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Pedidos`
--

CREATE TABLE `Pedidos` (
  `id` int(11) NOT NULL,
  `cliente` varchar(30) NOT NULL,
  `local` int(11) DEFAULT NULL,
  `complemento` varchar(30) DEFAULT NULL,
  `payment` enum('dinheiro','cartao') NOT NULL,
  `troco` decimal(10,0) DEFAULT NULL,
  `avaliacao` enum('1','2','3','4','5') DEFAULT NULL,
  `mensagem` varchar(256) DEFAULT NULL,
  `observacao` varchar(256) DEFAULT NULL,
  `vendedor` int(11) DEFAULT NULL,
  `data` datetime NOT NULL,
  `status` enum('aceitado','cancelado','recusado','entregado','aguardando') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Pedidos`
--

INSERT INTO `Pedidos` (`id`, `cliente`, `local`, `complemento`, `payment`, `troco`, `avaliacao`, `mensagem`, `observacao`, `vendedor`, `data`, `status`) VALUES
(18, 'Joãozinho', 1, NULL, 'dinheiro', NULL, NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', 2, '2018-01-24 21:38:55', 'aceitado'),
(29, 'Tiago', 3, NULL, 'dinheiro', NULL, NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-01-25 00:02:25', 'aguardando'),
(30, 'Tiagoo', 2, 'por ai', 'cartao', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-25 13:34:58', 'aguardando'),
(31, 'Felipaper', 3, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-27 18:57:06', 'aguardando'),
(32, 'Felipaper', 3, 'teste', 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-27 18:57:57', 'aguardando'),
(33, 'Felipaper', 2, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 10:03:25', 'aguardando'),
(34, 'Felipaper', 3, 'nada', 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:17:04', 'aguardando'),
(35, 'Felipaper', 3, NULL, 'cartao', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:24:00', 'aguardando'),
(36, 'Felipaper', 3, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:30:48', 'aguardando'),
(37, 'Felipaper', 3, NULL, 'cartao', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:36:26', 'aguardando'),
(38, 'Felipaper', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:37:00', 'aguardando'),
(39, 'Felipaper', 3, NULL, 'dinheiro', '100', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-28 23:37:59', 'aguardando'),
(40, 'Felipopudo', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-29 01:43:58', 'cancelado'),
(41, 'Felipopudo', 4, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', 2, '2018-03-30 13:43:11', 'recusado'),
(42, 'Felipopudo', 3, NULL, 'dinheiro', '100', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', 2, '2018-03-30 13:47:31', 'recusado'),
(43, 'Fjm', 3, NULL, 'cartao', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-30 14:18:13', 'aguardando'),
(44, 'Fjm', 3, NULL, 'dinheiro', '50', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-30 14:37:04', 'cancelado'),
(45, 'Fjm', 3, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-30 14:38:57', 'aguardando'),
(46, 'Fjm', 4, NULL, 'dinheiro', '2', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-30 15:26:23', 'aguardando'),
(47, 'Fjm', 3, NULL, 'dinheiro', '50', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:02:47', 'aguardando'),
(48, 'Fjm', 3, 'a pitanguinha nao me quer', 'dinheiro', '100', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:03:39', 'aguardando'),
(49, 'Fjm', 3, NULL, 'dinheiro', '100', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:13:17', 'aguardando'),
(50, 'Fjm', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:19:20', 'aguardando'),
(51, 'Fjm', 3, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:19:57', 'aguardando'),
(52, 'Naruto', 3, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 13:59:12', 'aguardando'),
(53, 'Naruto', 4, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:02:20', 'aguardando'),
(54, 'Naruto', 3, NULL, 'dinheiro', '50', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:04:40', 'aguardando'),
(55, 'Naruto', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:13:47', 'aguardando'),
(56, 'Naruto', 3, NULL, 'dinheiro', '50', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:36:02', 'aguardando'),
(57, 'Naruto', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:51:33', 'aguardando'),
(58, 'Naruto', 3, NULL, 'dinheiro', '20', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:55:45', 'aguardando'),
(59, 'Naruto', 3, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:57:41', 'aguardando'),
(60, 'Naruto', 1, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 14:58:13', 'aguardando'),
(61, 'Naruto', 1, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 15:00:41', 'aguardando'),
(62, 'Naruto', 3, NULL, 'dinheiro', '10', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 15:21:20', 'aguardando'),
(63, 'Naruto', 3, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', NULL, '2018-03-31 15:22:01', 'aguardando'),
(64, 'Naruto', 3, NULL, 'dinheiro', '0', NULL, NULL, 'o vendedor era muito feio acho que o nome dele era felipe\n', 2, '2018-03-31 15:55:41', 'recusado'),
(65, 'Naruto', 3, NULL, 'dinheiro', '0', NULL, NULL, NULL, NULL, '2018-03-31 16:18:16', 'aguardando');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Produtos`
--

CREATE TABLE `Produtos` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `preco` double NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `disponiveis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Produtos`
--

INSERT INTO `Produtos` (`id`, `nome`, `preco`, `img`, `disponiveis`) VALUES
(1, 'Trufa de morango', 1.5, 'http://bavierahaus.com.br/site/images/cardapio/doces/trufamorango.jpg', 3),
(2, 'Trufa de maracuja', 2.5, 'https://mariamestrecuca.files.wordpress.com/2015/04/dsc06998.jpg', 10),
(3, 'trufa de chocolate', 2, 'http://www.folhadomate.com//imagens/noticia/33710/38388-trufas_destaque.jpg', 23),
(4, 'Trufa de beijinho', 2, 'http://img.elo7.com.br/product/zoom/AA9F2E/mini-trufa-beijinho-tradicional.jpg', 5),
(5, 'Brownie', 6, 'https://avidadoce.com/wp-content/uploads/2017/04/receita-de-brownie-de-chocolate.jpg', 15),
(6, 'Pudim', 10, 'http://www.gazetadopovo.com.br/bomgourmet/wp-content/uploads/2016/05/pudim.jpg', 2),
(7, 'Pão de mel', 7, 'https://abrilmdemulher.files.wordpress.com/2016/09/receita-pao-mel-chocolate-branco-cachaca.jpg', 20),
(8, 'Kitkat', 5, 'https://www.hersheys.com/is/image/content/dam/kitkat/en_us/images/16x9_kit_kat_product3.jpg?hei=640', 16),
(9, 'Sonho', 3.5, 'https://abrilmdemulher.files.wordpress.com/2016/09/receita-sonho-doce-1.jpg', 4),
(10, 'Flor da pitanguinha', 99999999, 'http://generosoalimentos.com/cafe/imagens/produtos/recheado_morango.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Vendedores`
--

CREATE TABLE `Vendedores` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `lastActive` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Vendedores`
--

INSERT INTO `Vendedores` (`id`, `nome`, `user`, `pass`, `lastActive`) VALUES
(2, 'Tiago Hellebrandt', 'Tiago', '617696d1d6966137a44f229b45bdb6eccef19af9e898e17c74a2d699445c978b', '2018-03-31 16:16:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Itens`
--
ALTER TABLE `Itens`
  ADD PRIMARY KEY (`produto`,`pedido`),
  ADD KEY `pedido` (`pedido`);

--
-- Indexes for table `Locais`
--
ALTER TABLE `Locais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `Pedidos`
--
ALTER TABLE `Pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `local` (`local`),
  ADD KEY `vendedor` (`vendedor`);

--
-- Indexes for table `Produtos`
--
ALTER TABLE `Produtos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `Vendedores`
--
ALTER TABLE `Vendedores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Locais`
--
ALTER TABLE `Locais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Pedidos`
--
ALTER TABLE `Pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `Produtos`
--
ALTER TABLE `Produtos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `Vendedores`
--
ALTER TABLE `Vendedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `Itens`
--
ALTER TABLE `Itens`
  ADD CONSTRAINT `Itens_ibfk_1` FOREIGN KEY (`produto`) REFERENCES `Produtos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Itens_ibfk_2` FOREIGN KEY (`pedido`) REFERENCES `Pedidos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `Pedidos`
--
ALTER TABLE `Pedidos`
  ADD CONSTRAINT `Pedidos_ibfk_1` FOREIGN KEY (`local`) REFERENCES `Locais` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Pedidos_ibfk_3` FOREIGN KEY (`vendedor`) REFERENCES `Vendedores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
