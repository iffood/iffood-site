# IFFood

### Tecnologias utilizadas

- Front-end: HTML, CSS, JavaScript.
- Back-end: PHP.
- Banco de dados: MySQL.

### Avisos

- Utilizar [Bootstrap 3.3.6](http://blog.getbootstrap.com/2015/11/24/bootstrap-3-3-6-released/)
- Meta-Página na pasta `_elements` (Caso de erro (:bug:) nos links,provavelmente o caminho das pastas está errado)
- Utilizar Imagens de produtos com proporção 5:3

Variaveis user e pass estáticas na page controle.php (back-end favor fazer conexão do user e pass para a page)